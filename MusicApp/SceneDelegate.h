//
//  SceneDelegate.h
//  MusicApp
//
//  Created by ptnghia on 10/31/22.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

