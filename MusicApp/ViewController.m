//
//  ViewController.m
//  MusicApp
//
//  Created by ptnghia on 10/31/22.
//

#import "ViewController.h"
#import <AVKit/AVKit.h>
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *playPauseButton;
@property (weak, nonatomic) IBOutlet UISlider *sliderView;
@property (nonatomic,strong)AVAudioPlayer *player;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *loopButton;
@property (weak, nonatomic) IBOutlet UIView *viewImagee;
@property (weak, nonatomic) IBOutlet UILabel *labelNameSong;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@end

@implementation ViewController
NSArray *arraySong ;
NSTimer *spinTimer;
NSTimer *lengtSong;
NSArray*arrayImage;
NSArray*arrayNameSong;
float totalSecondSong = 247;
float currentSecond = 0;
int variableOfArray = 0;
int minNumberSong = 0;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self countArray];
    self.imageView.image = [UIImage imageNamed:arrayImage[variableOfArray]];
    self.labelNameSong.text = arrayNameSong[variableOfArray];
    self.backgroundImageView.image = [UIImage imageNamed:arrayImage[variableOfArray]];
    [self settingImageEffect];
    [self settingImageView];
    [_viewImagee addSubview:_imageView];
}

- (void)countArray {
    arraySong = [NSArray arrayWithObjects: @"audio0", @"audio1", @"audio2", @"audio3", nil];
    arrayImage = [NSArray arrayWithObjects: @"image1", @"image2", @"image3", @"image4", nil];
    arrayNameSong = [NSArray arrayWithObjects: @"First Aid Kit", @"Waiting For Five", @"Một Dề Nỗi Đau", @"Bên Trên Tầng Trên", nil];
}

- (void)settingImageEffect {
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleSystemChromeMaterialDark];
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.frame = _backgroundImageView.bounds;
    [_backgroundImageView addSubview:visualEffectView];
}

- (void)settingImageView {
    self.imageView.layer.cornerRadius  = self.imageView.frame.size.width/2;
    self.imageView.clipsToBounds = YES;
    self.imageView.layer.borderWidth = 6.0f;
    self.imageView.layer.borderColor = [UIColor blackColor].CGColor;
}

-(void)spinVoid {
    _viewImagee.transform = CGAffineTransformRotate(_viewImagee.transform, 0.001);
}

- (IBAction)onPlayPauseButtonClick:(id)sender {
    if ([spinTimer isValid]) {[spinTimer invalidate];
        [lengtSong invalidate];
        [_player pause];
        UIImage *btnImage = [UIImage systemImageNamed:@"play.fill"];
        [_playPauseButton setImage:btnImage forState:UIControlStateNormal];
    } else {
        spinTimer = [NSTimer scheduledTimerWithTimeInterval:.002 target: self selector:@selector(spinVoid) userInfo:nil repeats:YES];
        UIImage *btnImage = [UIImage systemImageNamed:@"pause.fill"];
        [_playPauseButton setImage:btnImage forState:UIControlStateNormal];
        if(!_player) {
            AVAudioSession *audioSession = [AVAudioSession sharedInstance];
            [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
            NSString *path = [[NSBundle mainBundle] pathForResource: arraySong[0] ofType:@"mp3"];
            NSURL *soundFileURL = [NSURL fileURLWithPath:path];
            _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
            [_player prepareToPlay];
            [_player play];
        } else {
            [_player play];
        }
        lengtSong = [NSTimer scheduledTimerWithTimeInterval: 1 target: self selector:@selector(caculatorLengtSong) userInfo:nil repeats:YES];
    }
}

- (void)caculatorLengtSong {
    currentSecond++;
    float percent= currentSecond/totalSecondSong * 100;
    [_sliderView setValue:percent ];
}

- (void)loopSong {
    _player.numberOfLoops = 1;
}

- (void)nextSong {
    variableOfArray++;
    if(variableOfArray > [arraySong count] -1) {
        NSString *path = [[NSBundle mainBundle] pathForResource: arraySong[3] ofType:@"mp3"];
        NSURL *soundFileURL = [NSURL fileURLWithPath:path];
        _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
        [_player prepareToPlay];
        [_player stop];
        currentSecond = minNumberSong;
        totalSecondSong = [self getDuration: soundFileURL];
        self.imageView.image = [UIImage imageNamed:arrayImage[3]];
        [self settingImageView];
        self.backgroundImageView.image = [UIImage imageNamed:arrayImage[3]];
        self.labelNameSong.text = arrayNameSong[3];
    } else {
        NSString *path = [[NSBundle mainBundle] pathForResource: arraySong[variableOfArray] ofType:@"mp3"];
        NSURL *soundFileURL = [NSURL fileURLWithPath:path];
        _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
        [_player prepareToPlay];
        [_player play];
        currentSecond = minNumberSong;
        totalSecondSong = [self getDuration: soundFileURL];
        self.imageView.image = [UIImage imageNamed:arrayImage[variableOfArray]];
        self.backgroundImageView.image = [UIImage imageNamed:arrayImage[variableOfArray]];
        self.labelNameSong.text = arrayNameSong[variableOfArray];
    }
}

- (void)backSong {
    variableOfArray--;
    if(variableOfArray < minNumberSong) {
        NSString *path = [[NSBundle mainBundle] pathForResource: arraySong[0] ofType:@"mp3"];
        NSURL *soundFileURL = [NSURL fileURLWithPath:path];
        _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
        [_player prepareToPlay];
        [_player stop];
        currentSecond = minNumberSong;
        totalSecondSong = [self getDuration: soundFileURL];
        self.imageView.image = [UIImage imageNamed:arrayImage[0]];
        [self settingImageView];
        self.backgroundImageView.image = [UIImage imageNamed:arrayImage[0]];
        self.labelNameSong.text = arrayNameSong[0];
    } else {
        NSString *path = [[NSBundle mainBundle] pathForResource: arraySong[variableOfArray] ofType:@"mp3"];
        NSURL *soundFileURL = [NSURL fileURLWithPath:path];
        _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
        [_player prepareToPlay];
        [_player play];
        currentSecond = minNumberSong;
        totalSecondSong = [self getDuration: soundFileURL];
        self.imageView.image = [UIImage imageNamed:arrayImage[variableOfArray]];
        [self settingImageView];
        self.backgroundImageView.image = [UIImage imageNamed:arrayImage[variableOfArray]];
        self.labelNameSong.text = arrayNameSong[variableOfArray];
    }
}

- (float)getDuration:(NSURL *)audioFileURL {
    AVURLAsset* audioAsset = [AVURLAsset URLAssetWithURL:audioFileURL options:nil];
    CMTime audioDuration = audioAsset.duration;
    float audioDurationSeconds = CMTimeGetSeconds(audioDuration);
    return audioDurationSeconds;
}

- (IBAction)loopButton:(id)sender {
    [self loopSong];
}

- (IBAction)nextButton:(id)sender {
    [self nextSong];
    if(variableOfArray > [arraySong count] -1) {
        _nextButton.enabled = NO;
    } else {
        _backButton.enabled = YES;
    }
}

- (IBAction)backButton:(id)sender {
    [self backSong];
    if(variableOfArray < minNumberSong) {
        _backButton.enabled = NO;
    } else {
        _nextButton.enabled = YES;
    }
}
@end
